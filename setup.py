import os

from setuptools import setup, find_packages


here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.rst')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()


with open(os.path.join(here, 'requirements.txt')) as f:
    req_lines = [line.strip() for line in f.readlines()]

    def fix_url_line(line):
        if '#egg=' in line:
            return line.rsplit('#egg=', 1)[1]
        return line

    requires = []
    for line in req_lines:
        if line:
            if "uvloop" in line and os.name == 'nt':
                continue
            else:
                requires.append(fix_url_line(line))


setup(
    name='kaiju-notifications',
    version='0.1.0',
    description='user notifications tables and services',
    long_description=README + '\n\n' + CHANGES,
    long_description_content_type='text/markdown; charset=UTF-8',
    author='antonnidhoggr@me.com',
    author_email='antonnidhoggr@me.com',
    url='https://bitbucket.org/market_app/kaiju-notifications',
    packages=find_packages(),
    package_dir={'kaiju_notifications': 'kaiju_notifications'},
    include_package_data=True,
    python_requires=">=3.6",
    install_requires=requires,
    license='LICENSE.txt',
    zip_safe=False,
    keywords=['notifications', 'elemento', 'kaiju'],
    classifiers=(
        'Programming Language :: Python :: 3.8',
    )
)
