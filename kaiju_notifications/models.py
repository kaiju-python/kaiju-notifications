"""
Data (SQL) models and functions.

Functions
---------

"""

import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as sa_pg

__all__ = ('create_notifications_table', 'notifications')


def create_notifications_table(table_name: str, metadata: sa.MetaData, *columns: sa.Column):
    """
    Notifications table.

    :param table_name: custom table name
    :param metadata: custom metadata object
    :param columns: additional columns
    """

    return sa.Table(
        table_name, metadata,

        sa.Column(
            'id', sa_pg.UUID, primary_key=True, server_default=sa.text("uuid_generate_v4()"),
            comment='Primary random ID.'
        ),
        sa.Column(
            'message', sa_pg.TEXT, nullable=False,
            comment='A message or a message loc string. Use args for formatting the template.'
        ),
        sa.Column(
            'format_args', sa_pg.JSONB, nullable=False,
            server_default=sa.text("'{}'::jsonb"),
            comment='Format arguments for message template.'
        ),
        sa.Column(
            'meta', sa_pg.JSONB, nullable=False,
            server_default=sa.text("'{}'::jsonb"),
            comment='Other data.'
        ),
        sa.Column('timestamp', sa_pg.TIMESTAMP, nullable=False, server_default=sa.text("now()")),
        sa.Column('active', sa_pg.BOOLEAN, nullable=False, server_default=sa.text("TRUE")),

        sa.Column('user_id', sa_pg.UUID, nullable=False),
        sa.Column('author_id', sa_pg.UUID, nullable=True),
        sa.Column('task_id', sa_pg.UUID, nullable=True),

        *columns,

        sa.Index('idx_notification_timestamp', 'timestamp'),
        sa.Index('idx_user_id', 'user_id'),
        sa.Index('idx_author_id', 'author_id')
    )


meta = sa.MetaData()
notifications = create_notifications_table('notifications', meta)
