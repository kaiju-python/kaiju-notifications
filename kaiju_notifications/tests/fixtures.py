import uuid

import pytest

from kaiju_tools.tests.fixtures import *
from kaiju_tools.rpc.tests.fixtures import *
from kaiju_db.tests.fixtures import *


@pytest.fixture
def notification() -> dict:
    return {
        'message': 'some.message',
        'format_args': {'value': 323},
        'meta': {'flag': True},
        'user_id': uuid.uuid4()
    }
